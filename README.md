# Coffee Shops API 

A Postman project to practice API calls and API testing. 
The documentation can be found here: http://webservice.toscacloud.com/training/swagger/ui/index under Shops.

A new Acces Key can be generated here: http://webservice.toscacloud.com/training/  !!Only available for 24 Hours!!

There are test scripts that verify the status code, but also if the response corresponds to the expectations.
The collection was run in Postman application, but also with Newman.

![Postman report](postman_report.jpg)
![CLI report](cli_report.jpg)

Final report was generated with HTML extra reporter
    Swagger_Coffee_Shops-2024-01-11-09-05-26-779-0.html

Total requests: 7
Total tests: 20, passed: 19, failed: 1

Technologies used:
Postman 10.21.14
Newman 6.1.0
